# Rest API Spawner

This is a spawner for jupyterhub based on slurm-api. The only endpoints required by this spawner are:

```yaml
  - method: GET
    path: "/job"
  - method: DELETE
    path: "/job/*"
  - method: PUT
    path: "/submit"
```

This repository is largely based on the BatchSpawner.
