# Copyright (c) Regents of the University of Minnesota
# Copyright (c) Michael Gilbert
# Copyright (c) Loic Hausammann
# Copyright (c) 2024 Urban Borstnik, ETH Zurich
# Distributed under the terms of the Modified BSD License.

import os
import pwd
from asyncio import sleep as asleep

import httpx
import requests
from async_generator import async_generator, yield_
from jinja2 import Template
from jupyterhub.spawner import Spawner
from slurm_api_schema.job import JobState, JobV2
from slurm_api_schema.submit import Submit
from tornado import gen
from traitlets import Float, Integer, List, Unicode, default


def format_template(template, *args, **kwargs):
    """Format a template, either using jinja2 or str.format().

    Use jinja2 if the template is a jinja2.Template, or contains '{{' or
    '{%'.  Otherwise, use str.format() for backwards compatability with
    old scripts (but you can't mix them).
    """
    if isinstance(template, Template):
        return template.render(*args, **kwargs)
    elif "{{" in template or "{%" in template:
        return Template(template).render(*args, **kwargs)
    return template.format(*args, **kwargs)


class RestApiSpawner(Spawner):
    """This class allows to spawn servers based on slurm-api"""

    software = List(
        [("Default", None)],
        help="List of scripts that can be loaded in the slurm script",
    ).tag(config=True)

    api_key_header = Unicode(
        "",
        help="Name of the header containing the API key for \
slurm api (value taken from environment API_KEY_VALUE)",
    ).tag(config=True)

    # override default since batch systems typically need longer
    start_timeout = Integer(30000000).tag(config=True)

    request_timeout = Integer(
        40, help="Timeout in seconds for the requests to slurm-api"
    ).tag(config=True)

    # override default server ip since batch jobs normally running remotely
    ip = Unicode(
        "0.0.0.0",  # nosec
        help="Address for singleuser server to listen at",
    ).tag(config=True)

    running_job_timeout = Integer(
        60,
        help="Timeout between slurm starting the job and the lab \
sending its first query to the hub",
    ).tag(config=True)

    poll_retry = Integer(
        5,
        help="Number of time to retry polling the job in case of timeout from slurm",
    ).tag(config=True)

    poll_sleep_between_retry = Integer(
        24,  # pol_retry * poll_sleep_between_retry ~ 2min
        help="Time to sleep in the poll function between two retries",
    ).tag(config=True)

    slurm_api_url = Unicode("", help="Url for the slurm-api service").tag(config=True)

    hostname_tld = Unicode(
        "",
        help="TLD to apply to the hostname (e.g. cnode02 to "
        "cnode02.slurm-dev.svc.cluster.local)",
    ).tag(config=True)

    default_account_endpoint = Unicode(
        "", help="Endpoints to get the default account of a user"
    ).tag(config=True)

    # all these req_foo traits will be available as substvars
    # for templated strings
    req_queue = Unicode(
        "",
        help="Queue name to submit job to resource manager",
    ).tag(config=True)

    req_memory = Unicode(
        "",
        help="Memory to request from resource manager",
    ).tag(config=True)

    req_nprocs = Unicode(
        "",
        help="Number of processors to request from resource manager",
    ).tag(config=True)

    req_ngpus = Unicode(
        "",
        help="Number of GPUs to request from resource manager",
    ).tag(config=True)

    req_runtime = Unicode(
        "",
        help="Length of time for submitted job to run",
    ).tag(config=True)

    req_partition = Unicode(
        "",
        help="Partition name to submit job to resource manager",
    ).tag(config=True)

    req_account = Unicode(
        "",
        help="Account name string to pass to the resource manager",
    ).tag(config=True)

    req_options = Unicode(
        "",
        help="Other options to include into job submission script",
    ).tag(config=True)

    req_oversubscribe = Unicode("", help="Enable job oversubscription.").tag(
        config=True
    )

    req_prologue = Unicode(
        "",
        help="Script to run before single user server starts.",
    ).tag(config=True)

    req_username = Unicode()

    @default("req_username")
    def _req_username_default(self):
        return self.user.name

    # Useful IF getpwnam on submit host returns correct info for exec host
    req_homedir = Unicode()

    @default("req_homedir")
    def _req_homedir_default(self):
        return pwd.getpwnam(self.user.name).pw_dir

    req_keepvars = Unicode()

    @default("req_keepvars")
    def _req_keepvars_default(self):
        return ",".join(self.get_env().keys())

    req_keepvars_extra = Unicode(
        help="Extra environment variables which should be configured, "
        "added to the defaults in keepvars, "
        "comma separated list.",
    )

    batch_script = Unicode(
        """#!/bin/bash
#SBATCH --output={{homedir}}/jupyterhub-logs/slurm-%j.log
#SBATCH --job-name=spawner-jupyterhub
#SBATCH --chdir={{homedir}}
#SBATCH --export={{keepvars}}
#SBATCH --get-user-env=L
#SBATCH --time={{runtime}}
#SBATCH --mem-per-cpu={{memory}}
#SBATCH --cpus-per-task={{nprocs}}
{% if account %}
#SBATCH --account={{account}}
{% endif %}
{% if ngpus %}
#SBATCH --gpus={{ngpus}}
{% endif %}
{% if gpu_ram %}
#SBATCH --gres=gpumem:{{gpu_ram}}g
{% endif %}
{% if oversubscribe %}
#SBATCH --oversubscribe
{% endif %}

{{env}}

set -euo pipefail

trap 'echo SIGTERM received' TERM
{{prologue}}
echo "jupyterhub-singleuser ended gracefully"
"""
    ).tag(config=True)

    # Variable used to know if the job already appeared in the queue
    in_queue = False

    # Raw output of job submission command unless overridden
    job_id = Integer(-1)

    # Will get the raw output of the job status command unless overridden
    job_reason = Unicode()

    # Will get the status of the job
    job_status = JobState.NONE

    # Number of errors querying the status
    job_query_error = Integer(0)

    # Prepare substitution variables for templates using req_xyz traits
    def get_req_subvars(self):
        reqlist = [t for t in self.trait_names() if t.startswith("req_")]
        subvars = {}
        for t in reqlist:
            subvars[t[4:]] = getattr(self, t)
        if subvars.get("keepvars_extra"):
            subvars["keepvars"] += "," + subvars["keepvars_extra"]
        return subvars

    async def get_account(self):
        username = self._req_username_default()
        response = requests.get(
            self.default_account_endpoint.format(user=username),
            timeout=self.request_timeout,
        )
        text = response.text.strip().strip("\n")
        self.log.info(f"Received '{text}' for user {username}")
        if response.status_code != requests.codes.ok:
            raise Exception("Failed to get default account (share)")
        return response.text

    async def _get_batch_script(self, **subvars):
        """Format batch script from vars"""
        # Could be overridden by subclasses, but mainly useful for testing
        return format_template(self.batch_script, **subvars)

    async def make_request(self, method, endpoint, **kwargs):
        transport = httpx.AsyncHTTPTransport(retries=3)
        headers = {} if "headers" not in kwargs else kwargs["headers"]
        if len(self.api_key_header) != 0:
            headers[self.api_key_header] = os.environ.get("API_KEY_VALUE")
        if "Content-Type" not in headers:
            headers["Content-Type"] = "application/json"

        async with httpx.AsyncClient(transport=transport) as client:
            response = await client.request(
                method, self.slurm_api_url + endpoint, headers=headers, **kwargs
            )
        return response

    async def submit_batch_script(self):
        subvars = self.get_req_subvars()

        if "DISABLE_DEFAULT_ACCOUNT" in os.environ:
            self.log.info("Default account is disabled")
        else:
            subvars["account"] = await self.get_account()

        # Add a few values for the batch script
        subvars["jupyterhub_args"] = " ".join(self.get_args())
        env = ""
        for key, value in self.get_env().items():
            env += f"export {key}='{value}'\n"
        # Add certificates
        for key, path in self.original_paths.items():
            with open(path, "r") as f:
                value = f.read()
            env += f"export {key}='{value}'\n"
        subvars["env"] = env

        if hasattr(self, "user_options"):
            subvars.update(self.user_options)
        for k, v in subvars.items():
            self.log.debug(f"user_options[{k}] = {v}")
        script = await self._get_batch_script(**subvars)

        username = self.user.name
        self.log.info(f"Spawner submitting script as {username}")
        response = await self.make_request(
            "PUT",
            "/submit",
            data=Submit(user=username, script=script).json(),
            timeout=self.request_timeout,
        )
        if response.status_code == httpx.codes.OK:
            self.log.info(f"Job successfully submitted for {username}")
            self.job_id = int(response.json())
        else:  # noqa: E722
            text = "Job submission failed: status={} text={}".format(
                response.status_code, response.text
            )

            self.log.error(text)
            raise RuntimeError(text)
        return self.job_id

    async def query_job_status(self):
        """Check job status, return JobStatus object."""
        if self.job_id == -1:
            self.job_reason = ""
            self.job_status = JobState.FAILED
            return JobState.FAILED

        self.log.info(f"Spawner querying job {self.job_id}")
        response = await self.make_request(
            "GET", f"/v2/job/{self.job_id}", timeout=self.request_timeout
        )
        if response.status_code != httpx.codes.OK:
            self.log.error(f"Error querying job {self.job_id}")
            self.job_reason = ""

            # Ensure that we don't keep checking it
            self.job_query_error += 1
            if self.job_query_error > 20:
                self.job_status = JobState.FAILED
                return JobState.FAILED

            self.job_status = JobState.NONE
            return JobState.NONE

        job = JobV2(**response.json())
        self.job_query_error = 0
        self.job_status = job.state
        self.job_reason = job.reason_raw
        return job.state

    async def cancel_batch_job(self):
        self.log.info(f"Cancelling job {self.job_id}")
        response = await self.make_request("DELETE", f"/job/{self.job_id}")
        if response.status_code != httpx.codes.OK:
            self.log.error(f"Failed to cancel job {self.job_id}")

    def load_state(self, state):
        """load job_id from state"""
        super(RestApiSpawner, self).load_state(state)
        self.job_id = state.get("job_id", -1)
        self.job_reason = state.get("job_reason", "")

    def get_state(self):
        """add job_id to state"""
        state = super(RestApiSpawner, self).get_state()
        if self.job_id:
            state["job_id"] = self.job_id
        if self.job_reason:
            state["job_reason"] = self.job_reason
        return state

    def clear_state(self):
        """clear job_id state"""
        super(RestApiSpawner, self).clear_state()
        self.job_id = -1
        self.job_reason = ""

    async def poll(self):
        """Poll the process"""
        for i in range(self.poll_retry):
            try:
                status = await self.query_job_status()
                break
            except httpx.ReadTimeout as e:
                self.log.exception(e)
                self.log.error(f"Failed processing job {self.job_id}")
                await asleep(self.poll_sleep_between_retry)

        if status in (JobState.PENDING, JobState.RUNNING, JobState.NONE):
            return None
        else:
            self.clear_state()
            return 1

    startup_poll_interval = Float(
        5,
        help="Polling interval (seconds) to check job state during startup",
    ).tag(config=True)

    async def start(self):
        """Start the process"""
        self.ip = self.traits()["ip"].default_value
        self.port = self.traits()["port"].default_value

        if self.server:
            self.server.port = self.port

        await self.submit_batch_script()

        # We are called with a timeout, and if the timeout expires this function will
        # be interrupted at the next yield, and self.stop() will be called.
        # So this function should not return unless successful, and if unsuccessful
        # should either raise and Exception or loop forever.
        if self.job_id == -1:
            raise RuntimeError("Jupyter batch job submission failure")
        while True:
            status = await self.query_job_status()
            if status == JobState.RUNNING:
                break
            elif status == JobState.PENDING:
                self.log.debug(f"Job {self.job_id} still pending")
            elif status == JobState.NONE:
                self.log.debug(f"Job {self.job_id} still unknown")
            else:
                self.log.warning(
                    f"Job {self.job_id} neither pending nor running.\n{self.job_reason}"
                )
                self.clear_state()
                raise RuntimeError(
                    "The Jupyter batch job has disappeared"
                    " while pending in the queue or died immediately"
                    " after starting."
                )
            await gen.sleep(self.startup_poll_interval)

        self.ip = self.job_reason + self.hostname_tld

        n = int(self.running_job_timeout // self.startup_poll_interval)
        for i in range(n + 1):
            if self.port != 0:
                break
            await gen.sleep(self.startup_poll_interval)

        if self.port == 0:
            raise RuntimeError(
                f"Port non valid, took too much time to startup: {self.job_id}"
            )

        self.db.commit()
        self.log.info(
            "Notebook server job {0} started at {1}:{2}".format(
                self.job_id, self.ip, self.port
            )
        )

        return self.ip, self.port

    async def stop(self, now=False):
        """Stop the singleuser server job.

        Returns immediately after sending job cancellation command if now=True,
        otherwise tries to confirm that job is no longer running."""

        self.log.info(f"Stopping server job {self.job_id}")
        await self.cancel_batch_job()
        if now:
            return
        for i in range(10):
            status = await self.query_job_status()
            if status not in (JobState.RUNNING, JobState.NONE):
                return
            await gen.sleep(1.0)
        if self.job_id:
            self.log.warning(
                "Notebook server job {0} at {1}:{2} possibly failed to "
                "terminate".format(self.job_id, self.ip, self.port)
            )

    def state_is_pending(self):
        ret = JobState.PENDING == self.job_status
        ret = ret or JobState.CONFIGURING == self.job_status
        if ret:
            self.in_queue = True
        return ret or not self.in_queue

    def state_is_running(self):
        ret = JobState.RUNNING == self.job_status
        ret = ret or JobState.COMPLETING == self.job_status
        return ret

    @async_generator
    async def progress(self):
        while True:
            if self.state_is_pending():
                # TODO Provide estimated time
                await yield_(
                    {
                        "message": (
                            "Pending in queue... If you are not able to "
                            "go further, please check the logs on Euler "
                            "(~/jupyterhub-logs/slurm-{}.log)"
                        ).format(self.job_id),
                    }
                )
            elif self.state_is_running():
                await yield_(
                    {
                        "message": "Cluster job running... waiting to connect",
                    }
                )
                return
            elif not self.in_queue:
                await yield_(
                    {
                        "message": "Waiting to appear in the queue",
                    }
                )
            else:
                await yield_(
                    {
                        "message": "Something wrong happened, please check your \
logs on Euler (~/jupyterhub-logs/slurm-*.log)",
                    }
                )
            await gen.sleep(5)

    def user_env(self, env):
        """get user environment"""
        env["USER"] = self.user.name
        home = pwd.getpwnam(self.user.name).pw_dir
        shell = pwd.getpwnam(self.user.name).pw_shell
        if home:
            env["HOME"] = home
        if shell:
            env["SHELL"] = shell
        return env

    def get_env(self):
        """Get user environment variables to be passed to the user's job

        Everything here should be passed to the user's job as
        environment.  Caution: If these variables are used for
        authentication to the batch system commands as an admin, be
        aware that the user will receive access to these as well.
        """
        env = super().get_env()
        env = self.user_env(env)
        return env

    async def move_certs(self, paths):
        # The scripts are written into the slurm script
        self.original_paths = paths
        return {
            "keyfile": ".config/euler/jupyterhub/jupyter.key",
            "certfile": ".config/euler/jupyterhub/jupyter.crt",
            "cafile": ".config/euler/jupyterhub/jupyter.ca",
        }
