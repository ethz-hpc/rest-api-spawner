.ONESHELL:
FLAKE8_ARGS=--max-line-length 88 --extend-ignore=E203

install-poetry:
	pip3 install poetry

update-poetry:
	poetry update

install-libraries:
	poetry install

test:
	set -e
	poetry run bandit -r slurm_api
	poetry run safety check

check-format:
	set -e
	poetry run black --check .
	poetry run isort --check-only --profile black .
	poetry run flake8 $(FLAKE8_ARGS) .

format:
	poetry run black .
	poetry run isort --profile black .
	poetry run flake8 $(FLAKE8_ARGS) .
